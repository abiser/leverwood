<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
do_action( 'pre_base_load' );

?>

<!doctype html>
<html <?php language_attributes(); ?>>

	<?php get_template_part( 'templates/head'); ?>
	<body <?php body_class(); ?>>
		<!--[if IE]>
			<div class="alert alert-warning">
				<?php _e( 'You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
			</div>
		<![endif]-->
		<?php do_action( 'get_header'); get_template_part( 'templates/header'); ?>
		<?php get_template_part( 'templates/main-menu'); ?>

		<div id="site" role="document">

			<?php include Wrapper\template_path(); ?>

			<?php if (Setup\display_sidebar()) : ?>
				<aside id="site_sidebar">
					<?php include Wrapper\sidebar_path(); ?>
				</aside><!-- site_sidebar -->
			<?php endif; ?>

			<?php do_action( 'get_footer'); get_template_part( 'templates/footer'); wp_footer(); ?>

		</div><!--site-->

		<?php
			while (have_posts()) : the_post();
			  $wp_forms_id = get_field('wp_forms_id');
			  $headline = get_field('headline');
			  $subText = get_field('subtext');
			  $button_title = get_field('button_title');

				if ($wp_forms_id != null):
				create_wp_form($wp_forms_id, $headline, $subText, $button_title);
				endif;
			endwhile;
		?>

		<script type="text/javascript">
		var DID=253496;
		</script>
		<script async src="//stats.sa-as.com/live.js"></script>

	</body>
</html>
