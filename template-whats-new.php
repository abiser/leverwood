<?php
/**
 * Template Name: What's New Template
 */
?>

<div class="subpage_header">
	<div class="subpage_header_inner">

	<div class="subpage_header_image" style="background-image: url('<?php echo get_the_post_thumbnail_url($post_id, 'large'); ?>');"></div>

	<?php get_template_part('templates/page', 'header'); ?>
	<div class="subpage_header_subtitle"><?php echo get_field('header_subtitle'); ?></div>

	</div><!--subpage_header_inner-->
	<?php get_template_part( 'templates/rainbow-bar'); ?>
</div><!--subpage_header-->

<nav class="tabbed_nav">

	<div class="container">
		<?php
		if (has_nav_menu('tabbed_nav_news')) :
		  wp_nav_menu(['theme_location' => 'tabbed_nav_news', 'menu_class' => 'nav']);
		endif;
		?>
	</div><!--container-->

</nav><!--tabbed_nav-->

<div class="container">
	<main id="site_main" class="mb-4">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">

				<?php while (have_posts()) : the_post(); ?>

					<div class="page_content">
				  	<?php get_template_part('templates/content', 'page'); ?>
				  </div><!-- page_content -->

				<?php endwhile; ?>

				<?php
					//What's New - Main Carousel
					if( have_rows('main_carousel') ): ?>
					<div class="single-item-with-dots slick-shadow" style="margin: 3rem 0;">

						<?php while( have_rows('main_carousel') ): the_row();

							$image = get_sub_field('image');
							$headline = get_sub_field('headline');
							$subheadline = get_sub_field('subheadline');
							$main_text = get_sub_field('main_text');
							$link = get_sub_field('link');
							$link_text = get_sub_field('link_text');

							?>

							<div class="card card-horizontal">
								<div class="card-img-left">
									<a href="<?php echo $link; ?>" class="card-img-left-inner" style="background-image: url(' <?php echo $image['url']; ?> ');"></a>
									<?php echo $image['url']; ?>
								</div>
								<div class="card-block">
									<h6 class="card-subtitle"><?php echo $subheadline ?></h6>
									<h4 class="card-title"><a href="<?php echo $link; ?>"><?php echo $headline ?></a></h4>
									<div class="card-text">
										<?php echo $main_text ?>
									</div>
									<a href="<?php echo $link; ?>" class="card-link"><?php echo $link_text; ?> <i class="ion-arrow-right-c"></i></a>
								</div>
							</div>

						<?php endwhile; ?>

					</div><!--Main Carousel-->
				<?php endif; ?>

				<?php
					//What's New - Secondary Carousel
					if( have_rows('secondary_carousel') ): ?>
					<div class="three-items-with-dots">

						<?php while( have_rows('secondary_carousel') ): the_row();

							$image = get_sub_field('image');
							$headline = get_sub_field('headline');
							$subheadline = get_sub_field('subheadline');
							$main_text = get_sub_field('main_text');
							$link = get_sub_field('link');
							$link_text = get_sub_field('link_text');

							?>

							<div class="item">
								<div class="card card-vertical matchHeight">
			    				<a href="<?php echo $link; ?>" class="card-img-top" style="background-image: url('<?php echo $image['url']; ?>');"></a>
									<div class="card-block">
										<h4 class="h5 card-title"><a href="<?php echo $link; ?>"><?php echo $headline ?></a></h4>
										<div class="card-text"><?php echo $main_text ?></div>
			    					<a href="<?php echo $link; ?>" class="card-link"><?php echo $link_text; ?> <i class="ion-arrow-right-c"></i></a>
									</div>
								</div>
							</div>

						<?php endwhile; ?>

					</div><!--Secondary Carousel-->
				<?php endif; ?>
			</div><!-- col -->
		</div><!-- row -->
	</main><!--site_main-->
</div><!--container-->
