<?php
/**
 * Template Name: News & Blog Template
 */
?>

<div class="subpage_header">
	<div class="subpage_header_inner">

	<div class="subpage_header_image" style="background-image: url('<?php echo get_the_post_thumbnail_url($post_id, 'large'); ?>');"></div>

	<?php get_template_part('templates/page', 'header'); ?>
	<div class="subpage_header_subtitle"><?php echo get_field('header_subtitle'); ?></div>

	</div><!--subpage_header_inner-->
	<?php get_template_part( 'templates/rainbow-bar'); ?>
</div><!--subpage_header-->

<nav class="tabbed_nav">

	<div class="container">
		<?php
		if (has_nav_menu('tabbed_nav_news')) :
		  wp_nav_menu(['theme_location' => 'tabbed_nav_news', 'menu_class' => 'nav']);
		endif;
		?>
	</div><!--container-->

</nav><!--tabbed_nav-->

<?php
	//User selectable field that controls the content displayed

	if ( get_field('select_category') != null ) {
		$category = get_field('select_category');

		echo '<div class="container">
						<div class="search-form-box">
							<div class="row">
								<div class="col-lg-10 offset-lg-1">	';
									get_search_form();
					echo '</div>
							</div>
						</div>
					</div>';

	} else {
		$category->name = "OICURMT"; //Change default returned variable
	}

?>

<div class="container">
	<main id="site_main" class="mb-4">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">

				<?php while (have_posts()) : the_post(); ?>

					<!-- Leave this element on one line. It will be hidden when empty -->
					<div class="page_content"><?php get_template_part('templates/content', 'page'); ?></div><!-- page_content -->

				  <?php
				  	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				  	$the_query = new WP_Query( array( 'category_name' => $category->name, 'paged' => $paged ) );
				  ?>
				  <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

			  		<div class="card card-horizontal">

			  			<div class="card-img-left">
		  					<a href="<?php the_permalink() ?>" class="card-img-left-inner" style="background-image: url(' <?php echo get_the_post_thumbnail_url($post_id, 'large'); ?> ');"></a>
		  				</div>

			  			<div class="card-block">
			  				<h6 class="card-subtitle"><?php echo $category->name; ?> <span>/ <?php echo get_the_date( 'M d, Y' ); ?></span></h6>
			  				<h4 class="h5 card-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
			  				<div class="card-text">
			  					<?php the_excerpt(__('(more…)')); ?>
			  				</div>
			  				<a href="<?php the_permalink() ?>" class="card-link">Read More <i class="ion-arrow-right-c"></i></a>
			  			</div>
			  		</div><!--card-->

				  <?php endwhile; wp_reset_postdata(); ?>

				  <?php
						//User selectable field that controls the content displayed
						if ( get_field('select_category') != null ) {

							echo '<div class="pagination_box clearfix">
							  <div class="pagination_box--prev">';
							  	next_posts_link( 'Older posts', $the_query->max_num_pages);
							echo '</div>';
							  echo '<div class="pagination_box--next">';
							  	previous_posts_link( 'Newer posts' );
							  echo '</div>';
						  echo '</div>';

						}

					?>

				<?php endwhile; ?>

			</div><!-- col -->
		</div><!-- row -->
	</main><!--site_main-->
</div><!--container-->
