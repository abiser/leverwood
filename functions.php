<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

function redirect_template() {
  if (get_page_template_slug() == 'template-first-child.php') {
    global $post;
    $pagekids = get_pages("child_of=".$post->ID."&sort_column=menu_order");
    wp_redirect(get_permalink($pagekids[0]->ID));
    exit();
  }

}
add_action( 'pre_base_load', 'redirect_template', 100 );

function alx_embed_html( $html ) {
    return '<div class="video-container">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'alx_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'alx_embed_html' );

function create_wp_form( $wp_forms_id, $headline, $subText, $button_title ) {
  echo '
    <div class="featured-form-modal__outer">
      <div class="featured-form-modal__wrapper">
        <div class="featured-form-modal">
          <div class="rainbow_bar rainbow_bar_top">
            <span class="red"></span>
            <span class="purple"></span>
            <span class="green"></span>
            <span class="yellow"></span>
            <span class="blue"></span>
          </div>
          <div class="modal__close-btn"><i class="icon ion-close-round"></i> Close</div>
          <h5 class="featured-form-modal__header">' . $headline . '</h5>
          <p class="t3">' . $subText . '</p>';
          ?>
          <?php
          wpforms_display((int)$wp_forms_id);
          echo '<a href="#" class="featured-form-modal__learn-more-btn btn btn-lg">' . $button_title . '</a>
        </div>
        <div class="modal__open-btn"><i class="icon ion-plus-round"></i></div>
      </div>
    </div>
  ';
}
