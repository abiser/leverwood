
<div class="subpage_header">
  <div class="subpage_header_inner">

  <div class="subpage_header_image" style="background-image: url('<?php echo get_the_post_thumbnail_url($post_id, 'medium_large'); ?>');"></div>

  <?php get_template_part('templates/page', 'header'); ?>
  </div><!--subpage_header_inner-->
  <?php get_template_part( 'templates/rainbow-bar'); ?>
</div><!--subpage_header-->

<div class="container">
  <main id="site_main" class="support_page mb-4">
    <div class="row">
      <div class="col-xl-10 offset-xl-1">

        <?php if (!have_posts()) : ?>
          <div class="alert alert-warning">
            <?php _e('Sorry, no results were found.', 'sage'); ?>
          </div>
          <?php get_search_form(); ?>
        <?php endif; ?>

        <?php while (have_posts()) : the_post(); ?>

          <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>

        <?php endwhile; ?>

        <?php the_posts_navigation(); ?>

      </div><!-- col -->
    </div><!-- row -->
  </main><!--site_main-->
</div><!--container-->
