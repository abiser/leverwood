<?php
/**
 * Template Name: Contact Us Template
 */
?>

<div class="subpage_header">
	<div class="subpage_header_inner">

	<div class="subpage_header_image" style="background-image: url('<?php echo get_the_post_thumbnail_url($post_id, 'large'); ?>');"></div>
		<?php get_template_part('templates/page', 'header'); ?>
	</div><!--subpage_header_inner-->
	<?php get_template_part( 'templates/rainbow-bar'); ?>
</div><!--subpage_header-->

<main id="site_main" class="mb-4">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4 offset-lg-1 col-md-5">
				<div class="main_page">
						
			  	<?php while (have_posts()) : the_post(); ?>
								
						<?php get_template_part('templates/content', 'page'); ?>

					<?php endwhile; ?>

				</div><!-- main_page -->
			</div><!-- col -->
			
			<?php 
				$contact_form = get_field('contact_form');
			?>

			<div class="col-md-6 contact_right_side_bar">
				<div class="row clearfix">
						
					<?php echo $contact_form ?>

				</div>
			</div><!-- col -->

		</div><!-- row -->
	</div><!-- container-fluid -->
</main><!--site_main-->

