<nav class="nav_primary">

	<div class="rainbow_bar rainbow_bar_top">
	  <span class="red"></span>
	  <span class="purple"></span>
	  <span class="green"></span>
	  <span class="yellow"></span>
	  <span class="blue"></span>
	</div>

	<div class="nav_primary_inner">
		<?php if (has_nav_menu( 'primary_navigation')) : wp_nav_menu([ 'theme_location'=>'primary_navigation', 'menu_class' => 'nav']); endif; ?>

		<div class="formWrapper wpforms-container-full">
			<h5 class="formWrapper__header">Login to your store</h5>
			<form name="loginForm" class="wpforms-form loginForm" action="https://vibenet.thalerus.com/Regency/Account/LoginLayer" onsubmit="javascript:document.getElementById('Password').value = Encryption.Sha512.Hmac.Base64(document.getElementById('LoginID').value.toLowerCase(),document.getElementById('txtPassword').value)">
				<div class="form-group">
					<label>Username</label>
					<input id="LoginID" name="LoginID" type="text" class="form-control" placeholder="Username" />
				</div>
				<div class="form-group">
				<label>Password</label>
					<input id="txtPassword" class="form-control" type="password" placeholder="Password" />
				</div>
				<input id="Password" name="Password" type="hidden" /><br />
				<input id="submit" class="btn btn-lg" type="submit" name="GO" value="Login" />
				<a href="#" class="toggle-form"><i class="ion ion-ios-arrow-thin-left"></i>Back to Menu</a>
			</form>
		</div>

		<?php if (has_nav_menu( 'primary_sub_navigation')) : wp_nav_menu([ 'theme_location'=>'primary_sub_navigation', 'menu_class' => 'nav']); endif; ?>

		<div class="nav_primary_footer">
			<div class="main_menu_contact">
				<p><span>Contact Us</span>1-844-726-6212</p>
				<i class="icon ion-android-call"></i>
			</div><!--main_menu_contact-->
			<a class="nav_primary_logo" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
		</div><!--nav_primary_footer-->

	</div><!--nav_primary_inner-->

</nav>
