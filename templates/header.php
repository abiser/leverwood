<header class="header_main">
	<div class="container-fluid">
		<a class="header_logo" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
		
		<div class="nav_main_toggle">
			<p class="main_toggle_text">MENU</p>
			<div class="main_toggle_lines">
			    <span class="line top"></span>
			    <span class="line mid"></span>
			    <span class="line bottom"></span>
		    </div>
		</div>
	</div><!--container-fluid-->
</header>