<?php while (have_posts()) : the_post(); ?>

<?php
	$hide_footer_contact_form = get_field('hide_footer_contact_form');
	if( !$hide_footer_contact_form ):
?>

	<div class="footer_contact_box">
		<div class="container-fluid">
			<div class="row">

				<div class="col-xl-3 offset-xl-2 col-lg-4 col-md-5 offset-md-1 col-sm-10 offset-sm-1">
					<div class="footer_contact_box_text">
						<div class="icon"></div>
						<h3 class="h4 header">Ready to get started?</h3>
						<p>Tell us a little about you, and a Regency360 representative will contact you to schedule a consultation.</p>
					</div>
				</div><!--col-->

				<div class="col-xl-4 col-lg-5 offset-lg-1 col-md-5 offset-md-0 col-sm-10 offset-sm-1">
					<div class="footer_contact_box_form">
						<?php dynamic_sidebar('sidebar-footer'); ?>
					</div>
				</div><!--col-->

			</div><!--row-->
		</div><!--container-fluid-->
	</div><!--footer_contact_box-->

<?php endif; ?>

<footer class="footer_main">

	<div class="rainbow_bar rainbow_bar_top">
		<span class="red"></span>
	  <span class="purple"></span>
	  <span class="green"></span>
	  <span class="yellow"></span>
	  <span class="blue"></span>
	</div>

	<div class="container-fluid">
		<div class="row">

			<div class="col-lg-3 col-md-3 col-sm-4 offset-md-1 col-xs-6">
				<div class="contact_details">
					<img src="/wp-content/themes/regency360/assets/images/logo-regency-bw.jpg" alt="Regency360 Logo" class="contact_details_logo">
					<div class="contact_details_text">
						<p>8024 Glenwood Avenue<br />
						Suite 200<br />
						Raleigh, NC 27612<br />
						1-844-726-6212</p>
					</div>
				</div>
			</div><!--col-->

			<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
				<div class="footer_nav">
					<h4 class="h6 nav_header">
						<?php
						if (has_nav_menu('footer_navigation_1')) :
							$nav_menu = wp_get_nav_menu_object(4); echo $nav_menu->name;
						endif;
						?>
					</h4>
					<?php
					if (has_nav_menu('footer_navigation_1')) :
					  wp_nav_menu(['theme_location' => 'footer_navigation_1', 'menu_class' => 'nav']);
					endif;
					?>
				</div>
			</div><!--col-->

			<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
				<div class="footer_nav">
					<h4 class="h6 nav_header">
						<?php
						if (has_nav_menu('footer_navigation_2')) :
							$nav_menu = wp_get_nav_menu_object(6); echo $nav_menu->name;
						endif;
						?>
					</h4>
					<?php
					if (has_nav_menu('footer_navigation_2')) :
					  wp_nav_menu(['theme_location' => 'footer_navigation_2', 'menu_class' => 'nav']);
					endif;
					?>
				</div>
			</div><!--col-->

			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 offset-sm-0 offset-xs-6">
				<div class="footer_nav">
					<h4 class="h6 nav_header">
						<?php
						if (has_nav_menu('footer_navigation_3')) :
							$nav_menu = wp_get_nav_menu_object(5); echo $nav_menu->name;
						endif;
						?>
					</h4>
					<?php
					if (has_nav_menu('footer_navigation_3')) :
					  wp_nav_menu(['theme_location' => 'footer_navigation_3', 'menu_class' => 'nav']);
					endif;
					?>
				</div>
			</div><!--col-->

		</div><!--row-->
	</div><!--container-fluid-->

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-10 offset-md-1 col-xs-12">
				<div class="footer_copyright">
					<p>&copy; <span class="post-year">2016</span> Regency 360 | All Rights Reserved</p>
				</div>
			</div>
		</div>
	</div><!--container-fluid-->

</footer>

<?php endwhile; ?>
