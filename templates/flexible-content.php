<?php
              
  // check if the flexible content field has rows of data
  if( have_rows('flexible_content_posts') ):
  
    // loop through the rows of data
    while ( have_rows('flexible_content_posts') ) : the_row();
    
      //Quote Box
      if( get_row_layout() == 'quote_block' ):
          
      $quote = get_sub_field('quote_block_quote');
      $author = get_sub_field('quote_block_author');
      
      echo '
      <div class="quote_box_block clearfix">
        <h4 class="quote_box_quote">'.$quote.'</h4>
        <p class="author">'.$author.'</p>
      </div>';
      
      //Rich Text Editor
        elseif( get_row_layout() == 'rich_text_editor_block' ):
      
      the_sub_field('rich_text_editor');

        //Callout Box
        elseif( get_row_layout() == 'callout_box_block' ):
      
      $alignment = get_sub_field('alignment');
      $headline = get_sub_field('headline');
      $headline_size = get_sub_field('headline_size');
      $text = get_sub_field('text');
      $link = get_sub_field('link');
      $link_text = get_sub_field('link_text');
      $background_image = get_sub_field('background_image');
      
      echo '
        <div class="callout_box_block callout_box_'. $alignment .' clearfix">
          <div class="callout_box_block_image" style="background-image: url('.$background_image.');"></div>
          <div class="callout_box_block_inner">
            <h4 class=" '.$headline_size.' header">'.$headline.'</h4>
            <p class="text">'.$text.'</p>
            <a href="'.$link.'" class="link btn btn-md btn-orange btn-round btn-shadow">'.$link_text.'</a>
          </div>
        </div>';
      
      //Fact Boxes
      elseif( get_row_layout() == 'case_study_fact_boxes' ):
      
      $box_1_title = get_sub_field('box_1_title');
      $box_1_icon = get_sub_field('box_1_icon');
      $box_2_title = get_sub_field('box_2_title');
      $box_2_icon = get_sub_field('box_2_icon');
      $box_3_title = get_sub_field('box_3_title');
      $box_3_icon = get_sub_field('box_3_icon');

      echo '  
      <div class="fact_boxes_block clearfix">
        <div class="fact_box">
          <i class="fact_box_icon '.$box_1_icon.' "></i>
          <p class="fact_box_headline">'.$box_1_title.'</p>
        </div>
        <div class="fact_box">
          <i class="fact_box_icon '.$box_2_icon.' "></i>
          <p class="fact_box_headline">'.$box_2_title.'</p>
        </div>
        <div class="fact_box">
          <i class="fact_box_icon '.$box_3_icon.' "></i>
          <p class="fact_box_headline">'.$box_3_title.'</p>
        </div>  
      </div>';
      
      //Floated Callout Box
      elseif( get_row_layout() == 'floated_callout_box' ):      
      
      $float_direction = get_sub_field('float_direction');
      $image = get_sub_field('image');
      $sub_headline = get_sub_field('sub_headline');
      $headline = get_sub_field('headline');
      $text = get_sub_field('text');
      $headline_size = get_sub_field('headline_size');
      $link = get_sub_field('link');
      $link_text = get_sub_field('link_text');

      echo '  
      <div class="floated_callout_box '.$float_direction.'">
        <div class="card card-vertical">
          <a href="'.$link.'" class="card-img-top" style="background-image: url('.$image.');"></a>
          <div class="card-block">';
            if ( $sub_headline != '' ) {
              echo '<h6 class="card-subtitle">' .$sub_headline. '</h6>';
            }
          echo '<h4 class="h5 card-title"><a href="'.$link.'">'.$headline.'</a></h4>
            <div class="card-text">'.$text.'</div>
            <a href="'.$link.'" class="card-link">'.$link_text.' <i class="ion-arrow-right-c"></i></a>
          </div>
        </div>
      </div>';

      //Insert Image
      elseif( get_row_layout() == 'insert_image' ):     
      
      $image = get_sub_field('image');
      $image_caption = get_sub_field('image_caption');

      echo '  
      <div class="insert_image_block">
        <img src="'.$image.'" class="img-fluid" />';
        if ( $image_caption != null ) {
          echo '<p>'.$image_caption.'</p><hr />';
        }
      echo '</div>';

      //Video(s) Block
      elseif( get_row_layout() == 'video_block' ): ?>

        <?php 
          $videos = get_sub_field('video');
          $videoFloat = '';
        ?>
        
        <?php //If there is only one video flow it inline with the content 
          if( count($videos) > 1 ) {
            echo '<div class="video_boxes_wrap clearfix"><div class="row">';
          } else {
            $videoFloat = 'video_boxes--single';
          }
        ?>
        

          <?php while( have_rows('video') ): the_row(); 
          
            $video_thumbnail = get_sub_field('video_thumbnail');
            $video_length = get_sub_field('video_length');
            $video_caption = get_sub_field('video_caption');
            $video_url = get_sub_field('video_url');
            
          ?>

          <div class="col-sm-6 <?php echo $videoFloat ?>">
            <div class="video_box media_thumb">
              <a data-fancybox="video" href="<?php echo $video_url ?>" class="media_thumb_link" style="background-image: url('<?php echo $video_thumbnail['url']; ?>')">
                <p class="media_thumb_runtime"><?php echo $video_length ?></p>
                <span class="media_thumb_icon">
                  <i class="ion-play" style="padding-left: .5rem;"></i>
                </span>
              </a>
              <p class="media_thumb_caption"><?php echo $video_caption ?></p>
            </div><!-- video_box -->
          </div><!-- col -->

          <?php endwhile; ?>

        <?php //If there is only one video flow it inline with the content
          if( count($videos) > 1 ) {
            echo '</div><!-- row --></div><!--video_boxes_wrap-->';
          }
        ?>
      
      <?php //Video Full Width Block
      elseif( get_row_layout() == 'video_full_width_block' ): ?>

      <div class="video_full_width_wrap clearfix">
          
          <?php

            $video = get_sub_field('video');
            $video_caption = get_sub_field('video_caption');

            echo '<div class="embed-responsive embed-responsive-16by9">'.$video.'</div>';
            if ( $video_caption != null ) {
              echo '<div class="caption">'.$video_caption.'</div><hr />';
            }

          ?>

      </div><!--video_boxes_wrap-->
      
      <? 
        //Photo Gallery Popup
        elseif( get_row_layout() == 'photo_block' ):     
      
        $photo_block_main_thumbnail = get_sub_field('photo_block_main_thumbnail');
        $photo_block_main_caption = get_sub_field('photo_block_main_caption');

      ?>

      <div class="insert_image_block media_thumb">
        <a class="media_thumb_link" href="<?php echo $photo_block_main_thumbnail['url'] ?>" data-fancybox="gallery-images" data-caption="<?php echo $photo_block_main_caption ?>" style="background-image: url('<?php echo $photo_block_main_thumbnail['url']; ?>')">
          <span class="media_thumb_icon">
            <i class="ion-arrow-resize"></i>
          </span>
        </a>
        <div class="media_thumb_caption"><?php echo $photo_block_main_caption ?></div>
        
        <?php 
          while( have_rows('photo_s') ): the_row();
          $photo_thumbnail = get_sub_field('photo_thumbnail');
          $photo_caption = get_sub_field('photo_caption');
        ?>
            
            <a class="hidden" href="<?php echo $photo_thumbnail[url]; ?>" data-fancybox="gallery-images" data-caption="<?php echo $photo_caption ?>"></a>

        <?php endwhile; ?>

      </div>

      <? 
        //Photo Carousel
        elseif( get_row_layout() == 'photo_carousel_block' ):     
      
        $photo_block_main_caption = get_sub_field('photo_block_main_caption');

      ?>
      
      <div class="photo-carousel">
        <div class="single-item-with-nav">
          
          <?php 
            while( have_rows('photo_s') ): the_row();
            $photo = get_sub_field('photo');
          ?>
          <div>
            <div class="picture" style="background-image: url('<?php echo $photo[url]; ?>')"></div>
          </div>

          <?php endwhile; ?>
          
        </div><!--single-item-with-nav-->

        <div class="caption"><?php echo $photo_block_main_caption ?></div>
        <hr>
 
      </div><!-- photo-carousel -->

      <? 
        //Logos Group
        elseif( get_row_layout() == 'logos_group' ):     

      ?>
      
      <div class="logos-group clearfix">
        <?php 
          while( have_rows('logos') ): the_row();
          $logo = get_sub_field('logo');
        ?>
      
        <div class="logo">
          <img src="<?php echo $logo[url]; ?>" alt="$logo[alt];" />
        </div>

        <?php endwhile; ?>
      </div><!-- logos-group -->

      <?php
      //Don't touch dummy 
      endif;

    endwhile;
  
    else : //no layouts found
  
  endif;
  
  ?>