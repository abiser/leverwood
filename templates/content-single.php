<div class="subpage_header">
	<div class="subpage_header_inner">
		<?php while (have_posts()) : the_post(); ?>
		<div class="subpage_header_image" style="background-image: url('<?php echo get_the_post_thumbnail_url($post_id, 'large'); ?>');"></div>

		<?php
			if ( (in_category('Case Study')) ) {
				echo '<div class="subpage_header_title"><h3 class="h6">Case Study</h3></div>';
			}
			else if ( (in_category('From the Blog')) ) {
				echo '<div class="subpage_header_title"><h3 class="h6">From the Blog</h3></div>';
			}
			else if ( (in_category('What\'s New')) ) {
				echo '<div class="subpage_header_title"><h3 class="h6">What\'s New</h3></div>';
			}
		?>

		<div class="page-header">
			<h1 class="h2"><?php the_title(); ?></h1>
		</div>
		<div class="subpage_header_meta">
			<?php
				if ( (in_category('What\'s New')) != true ) {
					get_template_part('templates/entry-meta');
				}
			?>
		</div>
		<?php endwhile; ?>
	</div>
	<?php get_template_part( 'templates/rainbow-bar'); ?>
</div><!--subpage_header-->

<nav class="tabbed_nav">

	<div class="container">
		<?php
		if (has_nav_menu('tabbed_nav_news')) :
		  wp_nav_menu(['theme_location' => 'tabbed_nav_news', 'menu_class' => 'nav']);
		endif;
		?>
	</div><!--container-->

</nav><!--tabbed_nav-->

<div class="social-sharing">
	<div class="container">
		<?php
			if(function_exists('social_warfare')):
		    social_warfare();
			endif;
		?>
	</div>
</div>

<?php
	//User selectable field that controls the content displayed
	$select_color = strtolower(get_field('select_color'));
?>

<div class="container">
	<main id="site_main" class="<?php echo 'page-color-' . $select_color; ?>">
		<div class="row">
			<div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">

				<?php while (have_posts()) : the_post(); ?>
				  <article <?php post_class(); ?>>

				    <div class="entry-content">
				      <?php the_content(); ?>

				      <?php include 'flexible-content.php'; ?>

				      <div class="post-categories-wrap">
				      	<div class="clearfix">
					    	<p class="hash_tag">#</p>
					    	<?php echo get_the_category_list(); ?>
					  	</div>
				      </div>

				    </div>

				    <footer>
				      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
				    </footer>

				    <?php //comments_template('/templates/comments.php'); ?>

				  </article>
				<?php endwhile; ?>

			</div><!--col-->
		</div><!--row-->

		<div class="row">
			<div class="col-lg-10 offset-lg-1">

				<div class="view_more_posts clearfix">

					<h6 class="headline">View More
						<?php
							if ( (in_category('Case Study')) ) { echo 'Case Studies'; }
							else if ( (in_category('From the Blog')) ) { echo 'Blog Posts'; }
							else if ( (in_category('What\'s New')) ) { echo 'What\'s New Posts'; }
						?>
					</h6>

					<?php $the_query = new WP_Query( array( 'post__not_in' => array($post->ID), 'category__in' => wp_get_post_categories(get_the_ID()), 'posts_per_page' => 3 ) ); ?>
					<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

						<div class="view_more_posts_item">
							<div class="card card-vertical matchHeight">
								<a href="<?php the_permalink() ?>" class="card-img-top" style="background-image: url('<?php echo get_the_post_thumbnail_url($post_id, 'large'); ?>');"></a>
								<div class="card-block">
									<h6 class="card-subtitle">
										<?php
											if ( (in_category('Case Study')) ) { echo 'Case Study'; }
											else if ( (in_category('From the Blog')) ) { echo 'From the Blog'; }
											else if ( (in_category('What\'s New')) ) { echo 'What\'s New'; }
										?>
										<span>/ <?php echo get_the_date( 'M d, Y' ); ?></span>
									</h6>
									<h4 class="h5 card-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
									<a href="<?php the_permalink() ?>" class="card-link">Read More <i class="ion-arrow-right-c"></i></a>
								</div>
							</div><!--card-->
						</div><!--view_more_posts_item-->
					<?php endwhile; wp_reset_postdata(); ?>
				</div><!--view_more_posts-->

			</div><!-- col -->
		</div><!-- row -->

	</main><!--site_main-->
</div><!--container-->
