<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
  <meta property="og:image" content="<?php echo get_site_url(); ?><?php echo get_the_post_thumbnail_url($post_id, 'large'); ?>" />

  <?php wp_head(); ?>

  <?php
    $seo_description = get_field('seo_description');
    $seo_keywords = get_field('seo_keywords');
  ?>

  <meta name="description" content="<?php echo $seo_description ?>">
  <meta name="keywords" content="<?php echo $seo_keywords ?>">

  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicons/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/apple-touch-icon-152x152.png" />
  <link rel="icon" type="image/png" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/favicon-196x196.png" sizes="196x196" />
  <link rel="icon" type="image/png" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/favicon-96x96.png" sizes="96x96" />
  <link rel="icon" type="image/png" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/favicon-16x16.png" sizes="16x16" />
  <link rel="icon" type="image/png" href="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/favicon-128.png" sizes="128x128" />
  <meta name="application-name" content="&nbsp;"/>
  <meta name="msapplication-TileColor" content="#FFFFFF" />
  <meta name="msapplication-TileImage" content="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/mstile-144x144.png" />
  <meta name="msapplication-square70x70logo" content="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/mstile-70x70.png" />
  <meta name="msapplication-square150x150logo" content="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/mstile-150x150.png" />
  <meta name="msapplication-wide310x150logo" content="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/mstile-310x150.png" />
  <meta name="msapplication-square310x310logo" content="<?php echo get_site_url(); ?>/wp-content/themes/regency360/favicon/mstile-310x310.png" />

  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
  <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
  <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js'></script>
  <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.4/isotope.pkgd.min.js'></script>
  <script type="text/javascript" src="https://vibenet.thalerus.com/Regency/Home/GetEmbeddedResource/Scripts/Common/Encryption.js"></script>

  <?php include_once("analyticstracking.php") ?>

</head>
