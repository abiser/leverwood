
  <?php if (get_post_type() === 'page') { 
    $hiddenClass = 'hidden-xs-up';
  } ?>

<div class="card card-horizontal">

  <div class="card-img-left <?php echo $hiddenClass ?>" >
    <a href="<?php the_permalink() ?>" class="card-img-left-inner" style="background-image: url(' <?php echo get_the_post_thumbnail_url($post_id, 'large'); ?> ');"></a>
  </div>
  
  <div class="card-block">
    <h6 class="card-subtitle"><?php if (get_post_type() === 'post') { get_template_part('templates/entry-meta'); } ?></h6>
    <h4 class="card-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
    <div class="card-text">
      <?php the_excerpt(__('(more…)')); ?>
    </div>
    <a href="<?php the_permalink() ?>" class="card-link">Read More <i class="ion-arrow-right-c"></i></a>
  </div>
</div><!--card-->