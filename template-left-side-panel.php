<?php
/**
 * Template Name: Left Side Panel Template
 */
?>

<main id="site_main" class="no-subheader mb-4">
		<div class="row" style="margin-left: 0; margin-right: 0;">
			<div class="col-lg-5 col-md-6 left-side-panel">
				<div class="main_page">

			  	<?php while (have_posts()) : the_post(); ?>

						<?php get_template_part('templates/content', 'page'); ?>

					<?php endwhile; ?>

				</div><!-- main_page -->
			</div><!-- col -->
			<div class="col-lg-7 col-md-6 right-side-panel" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->post_parent, 'large'); ?>');"></div>
		</div><!-- row -->
</main><!--site_main-->
