<div class="subpage_header">
  <div class="subpage_header_inner">

    <?php get_template_part('templates/page', 'header'); ?>
    <p><?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?></p>
  
    <div class="search-form-box">
      <?php get_search_form(); ?>
    </div>

  </div><!--subpage_header_inner-->
  <?php get_template_part( 'templates/rainbow-bar'); ?>
</div><!--subpage_header-->

<div class="container">
  <main id="site_main" class="support_page mb-4">
    <div class="row">
      <div class="col-xl-12"> 
        
        <?php if (!have_posts()) : ?>
          <div class="row">
              
            <?php $the_query = new WP_Query( array( 'category_name' => 'From the Blog', 'posts_per_page' => 3 ) ); ?>
            <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
              
              <div class="col-md-4">
                <div class="card card-vertical matchHeight">
                  <a href="<?php echo $link; ?>" class="card-img-top" style="background-image: url('<?php echo get_the_post_thumbnail_url($post_id, 'medium'); ?>');"></a>
                  <div class="card-block">
                    <h6 class="card-subtitle">From the Blog <span>/ <?php echo get_the_date( 'M d, Y' ); ?></span></h6>
                    <h4 class="h5 card-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
                    <div class="card-text">
                      <?php the_excerpt(__('(more…)')); ?>
                    </div>
                    <a href="<?php the_permalink() ?>" class="card-link card-link-bottom">Read More <i class="ion-arrow-right-c"></i></a>
                  </div>
                </div>
              </div><!--card-->
            
            <?php endwhile; wp_reset_postdata(); ?>
              
          </div><!--row-->
        <?php endif; ?>

        <?php while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/content', 'search'); ?>
        <?php endwhile; ?>

        <?php the_posts_navigation(); ?>
        
      </div><!-- col -->
    </div><!-- row -->
  </main><!--site_main-->
</div><!--container-->
