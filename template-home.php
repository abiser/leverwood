<?php
/**
 * Template Name: Home Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

  <?php if( have_rows('homepage_boxes') ): ?>
	<div class="page-component homepage_header_boxes clearfix">

		<div class="mobile_slides">

		<?php while( have_rows('homepage_boxes') ): the_row();

			$image = get_sub_field('image');
			$headline = get_sub_field('headline');
			$sub_headline = get_sub_field('sub_headline');
			$link = get_sub_field('link');

			?>

			<div class="homepage_header_box">

				<?php if( $link ): ?>
					<a class="box_link" href="<?php echo $link; ?>">
				<?php endif; ?>

					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />

					<div class="header_box_text">
						<div class="inner_cell">
							<h2 class="h4 headline">
								<?php echo $headline ?>
							</h2>
							<p class="subheadline"><?php echo $sub_headline ?><i class="ion-ios-play"></i></p>
						</div>
					</div>

					<div class="bg-color"></div>

				<?php if( $link ): ?>
					</a>
				<?php endif; ?>

			</div><!--homepage_header_box-->

		<?php endwhile; ?>

		</div><!--mobile_slides-->

			<?php
				$custom_message_box_headline = get_field('custom_message_box_headline');
				$custom_message_box_text = get_field('custom_message_box_text');
				$custom_message_box_link = get_field('custom_message_box_link');
			?>

			<div class="homepage_header_box custom_message">
					<a class="box_link" href="<?php echo $custom_message_box_link ?>">
						<span class="arrow_link"><i class="ion-android-arrow-dropright"></i></span>
						<div class="custom_message_inner">
							<div class="custom_message_inner_cell">
								<h1 class="h4 heading"><?php echo $custom_message_box_headline ?></h1>
								<hr />
								<div class="text"><?php echo $custom_message_box_text ?></div>
							</div>
						</div>
					</a>
			</div><!--homepage_header_box-->

	</div><!--homepage_header_boxes-->
	<?php endif; ?>

	<div class="page-component homepage_company_stores clearfix">

		<?php
			$cs_headline = get_field('cs_headline');
			$cs_text = get_field('cs_text');
			$cs_carousel_screens = get_field('cs_carousel_screens');
		?>

		<div class="screens_header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<h2 class="h3"><?php echo $cs_headline ?></h2>
						<hr>
						<div class="text"><?php echo $cs_text ?></div>
					</div><!--col-->
				</div><!--row-->
			</div><!--container-fluid-->
		</div><!--header-->


		<?php if( have_rows('home_carousel_screens') ): ?>
		<div class="screens_carousels">

				<div class="screens_computer single-item">
					<?php while( have_rows('home_carousel_screens') ): the_row();
						$desktop_screen = get_sub_field('desktop_screen'); ?>
					<img src="<?php echo $desktop_screen['url']; ?>" alt="<?php echo $desktop_screen['alt'] ?>" />
					<?php endwhile; ?>
				</div><!--screens_computer-->

				<div class="screens_phone single-item">
						<?php while( have_rows('home_carousel_screens') ): the_row();
							$mobile_screen = get_sub_field('mobile_screen'); ?>
						<img src="<?php echo $mobile_screen['url']; ?>" alt="<?php echo $desktop_screen['alt'] ?>" />
					<?php endwhile; ?>
				</div><!--screens_phone-->

		</div><!--screens_carousels-->
		<?php endif; ?>

		<div class="company_stores_features">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-10 offset-lg-1">

					<?php if( have_rows('features_boxes') ): ?>

						<?php while( have_rows('features_boxes') ): the_row();

							$featured_box_icon = get_sub_field('featured_box_icon');
							$featured_box_icon_color = get_sub_field('featured_box_icon_color');
							$featured_box_background_icon_size = get_sub_field('featured_box_background_icon_size');
							$featured_box_headline = get_sub_field('featured_box_headline');
							$featured_box_text = get_sub_field('featured_box_text');
							$featured_box_link = get_sub_field('featured_box_link');

						?>

						<a href="<?php echo $featured_box_link ?>" class="features_item">
							<div class="icon"
								style="background-image: url(' <?php echo $featured_box_icon['url'] ?> ');
								       background-size: <?php echo $featured_box_background_icon_size ?> auto;
											 background-color: <?php echo $featured_box_icon_color ?>;
								"></div>
							<h4 class="h6 header"><?php echo $featured_box_headline ?></h4>
							<div class="text"><?php echo $featured_box_text ?></div>
							<p class="link" style="color: <?php echo $featured_box_icon_color ?>;">LEARN MORE <i class="ion-arrow-right-b"></i></p>
						</a>

						<?php endwhile; ?>

					<?php endif; ?>

					</div><!--col-->
				</div><!--row-->
			</div><!--container-fluid-->
		</div><!--company_stores_features-->

	</div><!--homepage_company_stores-->

	<div class="homepage_regency_innovations">
		<div class="container-fluid">

			<div class="header-box">
				<h2 class="headline h5">Regency360 <span>Innovations</span></h2>
			</div>

			<div class="row">
				<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-12 col-xs-12">

					<div class="single-item-with-dots slick-shadow">

						<?php $the_query = new WP_Query( array( 'category_name' => 'Case Study', 'posts_per_page' => 3 ) ); ?>

						<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

						<div>
							<div class="card card-horizontal">
								<div class="card-img-left">
									<a href="<?php the_permalink() ?>" class="card-img-left-inner" style="background-image: url(' <?php echo get_the_post_thumbnail_url($post_id, 'large'); ?> ');"></a>
								</div>
								<div class="card-block">
									<h6 class="card-subtitle">Case Study</h6>
									<h4 class="card-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
									<div class="card-text">
										<?php the_excerpt(__('(more…)')); ?>
									</div>
									<a href="<?php the_permalink() ?>" class="card-link">Read More <i class="ion-arrow-right-c"></i></a>
								</div>
							</div>
						</div>

						<?php endwhile; wp_reset_postdata(); ?>

					</div><!--single-item-with-dots-->

				</div><!--col-->
			</div><!--row-->

			<div class="row">
				<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-12 col-xs-12">

					<div class="regency_innovations_news">
					<?php $the_query = new WP_Query( array( 'category_name' => 'From the Blog', 'posts_per_page' => 3 ) ); ?>
					<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

						<div class="regency_innovations_news_item">
							<div class="card card-vertical matchHeight">
								<div class="card-block">
									<h6 class="card-subtitle">From the Blog <span>/ <?php echo get_the_date( 'M d, Y' ); ?></span></h6>
									<h4 class="h5 card-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
									<div class="card-text">
										<?php the_excerpt(__('(more…)')); ?>
									</div>
									<a href="<?php the_permalink() ?>" class="card-link card-link-bottom">Read More <i class="ion-arrow-right-c"></i></a>
								</div>
							</div><!--card-->
						</div><!--regency_innovations_news_item-->

					<?php endwhile; wp_reset_postdata(); ?>
					</div><!--regency_innovations_news-->

				</div><!--col-->
			</div><!--row-->

		</div><!--container-fluid-->
	</div><!--homepage_regency_innovations-->

  <div class="container-fluid">
    <div class="social_board_main">
      <?php if (function_exists('social_board')) echo social_board( array( 'id' => 1955, 'type' => 'wall' ) ); ?>
    </div>
  </div>

<?php endwhile; ?>
