/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {

        function animationDurationInterval(elem, num) {
        	var time = "0";
        	$(elem).each( function() {
        		time = parseFloat(time) + parseFloat(num);
        		$(this).css("animation-duration", time + "s");
        	});
        }

        function matchNewsHeight() {

            var elements = $('.card.card-vertical.matchHeight');

            $(elements).css("height", "auto");

            var tallest = 0;
            var tallestElem;

            $(elements).each( function(index) {

               if ( $(this).outerHeight() > tallest ) {
                  tallest = $(this).outerHeight();
                  //return tallestElem = $(this);
                  //return tallest;
               }

            });

            $(elements).css("height", tallest + 'px');

        }

        animationDurationInterval('.nav_primary ul li', 0.1);

        $(window).on('load', function(e) {
          matchNewsHeight();

          $('.social-sharing a').on('click', function(e) {
          	e.preventDefault();
          });

          //Show login form
          $('.nav_primary .menu-login').on('click', function(e) {
            e.preventDefault();
            $('.nav_primary').toggleClass('showForm');
          });

          //Hide login form
          $('.nav_primary .formWrapper a.toggle-form').on('click', function(e) {
            e.preventDefault();
            $('.nav_primary').toggleClass('showForm');
          });

        });

        $(window).on('resize', function(e) {
          matchNewsHeight();
        });

        $('.nav_main_toggle').click( function() {
        	$('.nav_primary_inner').toggle();
        	$(this).toggleClass("active");
        	$('.header_logo').fadeToggle(300);
        	$("body").toggleClass("menu-open");
        });

        $('.home .screens_carousels .single-item').slick({
          arrows: false,
          fade: true,
          infinite: true,
          pauseOnHover: false,
          speed: 1000,
          autoplay: true,
          autoplaySpeed: 4000,
          touchMove: false,
          draggable: false
        });

      	$('.single-item-with-dots').slick({
      		arrows: false,
      		dots: true,
      		infinite: true,
      		pauseOnHover: false,
      		speed: 1000,
      		autoplay: true,
      		autoplaySpeed: 6000,
      		touchMove: true,
      		draggable: true,
      		adaptiveHeight: true
      	});

        $('.three-items-with-dots').slick({
          slidesToShow: 3,
          slidesToScroll: 3,
          arrows: false,
          dots: true,
          infinite: false,
          pauseOnHover: true,
          speed: 1000,
          autoplay: true,
          autoplaySpeed: 6000,
          touchMove: false,
          draggable: false,
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });

        $('.logos-group').slick({
          dots: true,
          arrows: false,
          infinite: false,
          speed: 300,
          slidesToShow: 4,
          slidesToScroll: 4,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });

        $('.single-item-with-nav').slick({
          arrows: true,
          dots: false,
          infinite: true,
          pauseOnHover: true,
          speed: 1000,
          autoplay: true,
          autoplaySpeed: 6000,
          touchMove: true,
          draggable: true,
          adaptiveHeight: true,
          prevArrow: '<div class="slick-prev"><i class="ion-ios-arrow-thin-left"></i>',
          nextArrow: '<div class="slick-next"><i class="ion-ios-arrow-thin-right"></i>'
        });

        //Main navigation mobile toggles
        $('#menu-main-navigation li').each(function() {
          $(this).has('ul.sub-menu').append('<i class="menu-item-toggle ion-android-arrow-dropdown"></i>');
        });

        $('#menu-main-navigation li .menu-item-toggle').click(function() {
          $(this).prev('ul.sub-menu').toggle();
        });

        //Social sharing
        $(".social-sharing .facebook a").on("click",function(){
          var url = $(location).attr('href');
          window.open("https://www.facebook.com/sharer/sharer.php?u=" + url, "pop", "width=600, height=400, scrollbars=no");
          return false;
        });

        $(".social-sharing .twitter a").on("click",function(){
          var url = $(location).attr('href');
          window.open("https://twitter.com/intent/tweet?&url=" + url, "pop", "width=600, height=400, scrollbars=no");
          return false;
        });

        $(".social-sharing .linkedin a").on("click",function(){
          var url = $(location).attr('href');
          window.open("https://www.linkedin.com/shareArticle?url=" + url, "pop", "width=600, height=400, scrollbars=no");
          return false;
        });

        $(".social-sharing .google a").on("click",function(){
          var url = $(location).attr('href');
          window.open("https://plus.google.com/share?url=" + url, "pop", "width=600, height=400, scrollbars=no");
          return false;
        });

        // Delayed pop up forms
        $(document).ready(function() {
          // Modal close Btn
          $('.featured-form-modal__wrapper .modal__close-btn').click(function(e) {
            e.preventDefault();
            $('.featured-form-modal').removeClass('featured-form-modal--visible');
            $('.modal__open-btn').addClass('modal__open-btn--active');
            $('body').removeClass('body--modal-open');
          });
          // Modal open Btns
          $('.featured-form-modal__learn-more-btn').click(function(e) {
            e.preventDefault();
            // $('#wpforms-2173-field_0-container input').focus();
            $('body').addClass('body--modal-open');
            $('.featured-form-modal').addClass('featured-form-modal--visible');
            $('.modal__open-btn').removeClass('modal__open-btn--active');
            // $(window).scrollTop(0);
          });
          $('.featured-form-modal__wrapper .modal__open-btn').click(function(e) {
            e.preventDefault();
            // $('#wpforms-2173-field_0-container input').focus();
            $('.featured-form-modal').addClass('featured-form-modal--visible');
            $('.modal__open-btn').removeClass('modal__open-btn--active');
            $('body').addClass('body--modal-open');
            // $(window).scrollTop(0);
          });
        });

        $(window).on('load', function(e) {

          //Insert page header text in hidden field
          var headerText = $('.page-header h1').text();
          $('.wpforms-field.field-page-name input').val(headerText);

          var hasFormBeenSubmitted = window.location.href.indexOf('#wpforms-') > -1;
          var formPopUpDelay = 6000;
          if (hasFormBeenSubmitted) {
            formPopUpDelay = 500;
          }
          setTimeout(function() {
            $('.featured-form-modal').addClass('featured-form-modal--visible');
          }, formPopUpDelay);

        });

      },
      finalize: function() {

        var copyright = new Date();
        $('.footer_copyright .post-year').html( copyright.getFullYear() );

      }
    },
    // Home page
    'home': {
      init: function() {

      	function formatHomeboxes() {

	      	if ( $(window).width() <= 479 ) {
	      		$('.homepage_header_boxes .mobile_slides').slick({
	  				arrows: false,
	  				fade: false,
	  				//dots: true,
	  				infinite: true,
	  				speed: 300,
	  				autoplay: true,
	  				autoplaySpeed: 6000,
	  				touchMove: true,
	  				draggable: true
	  			});
	      	} else {
	      		if( $('.mobile_slides').hasClass('slick-initialized')) {
	      			$('.homepage_header_boxes .mobile_slides').slick('unslick');
	      		}
	      	}

	      	$('.homepage_header_box').each( function() {

      			$(this).find('img').removeClass("stretch_img_horiz");

      			var boxWidth = $(this).width();
      			var boxImgWidth = $(this).find('img').width();

      			if ( boxImgWidth > boxWidth ) {

      				var newPos = (boxImgWidth - boxWidth) / 2;
      				$(this).find('img').css("left", "-" + newPos.toFixed() + "px" );

      			} else {
      				$(this).find('img').addClass("stretch_img_horiz");
      			}

      		});

      	}


      	$(window).on( 'load', function() {

      		formatHomeboxes();

      	});

      	$(window).on( 'resize', function() {

      		$('.homepage_header_box').find('img').removeClass('stretch-horz');
      		$('.homepage_header_box').find('img').css("left", "0px");
      		formatHomeboxes();

      	});

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    'page_template_template_subpage_second_level': {
      init: function() {

        function resizeRightSidebarNav() {
          var $element = $('.nav_right_side_bar .nav_right_side_bar_inner');
          $($element).css('width', $($element).parent().width());
        }

        $(document).ready(function() {

          var $sticky = $('.nav_right_side_bar .nav_right_side_bar_inner');
          var $stickyrStopper = $('.footer_contact_box');
          if (!!$sticky.offset()) { // make sure ".sticky" element exists

            var generalSidebarHeight = $sticky.innerHeight();
            var stickyTop = $sticky.offset().top;
            var stickOffset = 120;
            var stickyStopperPosition = $stickyrStopper.offset().top - 60;
            var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
            var diff = stopPoint + stickOffset;

            $(window).scroll(function(){ // scroll event
              var windowTop = $(window).scrollTop(); // returns number

              if (stopPoint < windowTop) {
                  //$sticky.css({ position: 'absolute', top: diff });
                  $sticky.css({ opacity: 0 });
              } else if (stickyTop < windowTop+stickOffset) {
                  $sticky.css({ position: 'fixed', opacity: 1, top: stickOffset });
              } else {
                  $sticky.css({position: 'absolute', top: 'initial'});
              }
            });

          }
        });

        $(window).load(function() {
          resizeRightSidebarNav();
        });

        $(window).resize(function() {
          resizeRightSidebarNav();
        });

        //Change fancybox aspect ratio
        $('[data-fancybox="video"]').fancybox({
          afterLoad : function( instance, current ) {

             // Remove scrollbars and change background
            current.$content.css({
              overflow   : 'visible',
              background : '#000'
            });

          },
          onUpdate : function( instance, current ) {
            var width,
                height,
                ratio = 16 / 9,
                video = current.$content;

            if ( video ) {
              video.hide();

              width  = current.$slide.width();
              height = current.$slide.height() - 100;

              if ( height * ratio > width ) {
                height = width / ratio;
              } else {
                width = height * ratio;
              }

              video.css({
                width  : width,
                height : height
              }).show();

            }
          }
        });

      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    //Posts
    'single': {
      init: function() {

        function matchFactsHeight() {

            var elements = $('.fact_box');

            $(elements).css("height", "auto");

            var tallest = 0;
            var tallestElem;

            $(elements).each( function(index) {

               if ( $(this).outerHeight() > tallest ) {
                  tallest = $(this).outerHeight();
                  //return tallestElem = $(this);
                  //return tallest;
               }

            });

            $(elements).css("height", tallest + 'px');

        }

        $(window).on('load', function(e) {
          matchFactsHeight();
        });
        $(window).on('resize', function(e) {
          matchFactsHeight();
        });

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
