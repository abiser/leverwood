<?php
/**
 * Template Name: Subpage Second Level Template
 */
?>

<div class="subpage_header">
	<div class="subpage_header_inner">

	<div class="subpage_header_image" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->post_parent, 'large'); ?>');"></div>
		
		<div class="container">
			<div class="page-header">
				<h1><?php echo get_the_title( $post->post_parent ); ?></h1>
			</div>
			<div class="subpage_header_text"><?php echo get_post_field('post_content', $post->post_parent); ?></div>
		</div><!-- container -->
	</div><!--subpage_header_inner-->
	<?php get_template_part( 'templates/rainbow-bar'); ?>
</div><!--subpage_header-->

<main id="site_main" class="mb-4">
	<div class="container">
		<div class="row">
			<div class="col-xl-9 col-md-8 col-xs-12">
				<div class="main_page">
						
					<div class="single-item-with-nav-wrap">
						<?php get_template_part('templates/page', 'header'); ?>

						<?php 
							//What's New - Main Carousel
							if( have_rows('main_image_area') ): ?>
							<div class="single-item-with-nav">
						
								<?php while( have_rows('main_image_area') ): the_row(); 
									$image = get_sub_field('image');
								?>
									
									<div class="item" style="background-image: url(' <?php echo $image['url']; ?> ');"></div>

								<?php endwhile; ?>
							
							</div><!--single-item-with-nav-->
						<?php endif; ?>
					</div><!-- single-item-with-nav-wrap -->	

					<?php 
						if ( get_field('bullet_list_sidebar') != null ) {

								$bullet_list_sidebar = get_field('bullet_list_sidebar');
								$col_left_class = "col-xl-7 col-xs-12";
								$col_right_class = "col-xl-5 col-xs-12";

						} else {
								$col_left_class = "col-xs-12";
								$col_right_class = "col-md-5 hidden-xs-up";
						}
					?>

					<div class="main_content">
						<div class="row">
							<div class="<?php echo $col_left_class ?>">
								<?php while (have_posts()) : the_post(); ?>
									
									<?php get_template_part('templates/content', 'page'); ?>

						      <?php include 'templates/flexible-content.php'; ?>

								<?php endwhile; ?>

							</div><!-- col -->
							
							<div class="<?php echo $col_right_class ?>">
								<div class="bullet_list_sidebar">
									<?php echo $bullet_list_sidebar ?>
								</div><!-- bullet_list_sidebar -->
							</div><!-- col -->

						</div><!-- row -->
					</div><!-- main_content -->

				</div><!-- main_page -->
			</div><!-- col -->

			<nav class="col-xl-3 col-md-4 nav_right_side_bar">
				<div class="nav_right_side_bar_inner">
					<h6 class="nav_right_side_bar__header"><?php echo get_the_title( $post->post_parent ); ?></h6>
					<ul class="nav">
						<?php
							
							$page_title = get_the_title($post);
							$pages = get_pages("child_of=".$post->post_parent."&sort_column=menu_order");	
		 
							foreach ( $pages as $page ) {

								//Replace	odd characters
								$characters = array("&", "-", " ", "#038;");
								$post_title_new = str_replace($characters, "", $page->post_title);
								$page_title_new = str_replace($characters, "", $page_title);	

								$post_title_new === $page_title_new ? $navClass = "active" : $navClass = "";

						  	$nav = '<li class="' . $navClass . '"><a href="' . get_page_link( $page->ID ) . '">';
								$nav .= $page->post_title;
								$nav .= '</a></li>';
								echo $nav;

						  }

						?>
					</ul>
				</div>	
			</nav><!-- col -->

		</div><!-- row -->
	</div><!-- container -->
</main><!--site_main-->

